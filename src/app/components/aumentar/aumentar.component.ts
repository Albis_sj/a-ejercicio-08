import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aumentar',
  templateUrl: './aumentar.component.html',
  styleUrls: ['./aumentar.component.css']
})
export class AumentarComponent implements OnInit {

  number:number = 1;
  text: string = 'Todo Correcto';
  textErr: string = 'NO PUEDE SOBREPASAR EL LÍMITE';

  constructor() { }

  ngOnInit(): void {
  }
  aumentar(){
    this.number = this.number +1
    if (this.number === 50){
      this.text = this.textErr
    }
 }

  reducir(){
    this.number = this.number -1;
    if (this.number === 0){
      this.text = this.textErr
    }
  }

}
